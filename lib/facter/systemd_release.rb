# frozen_string_literal: true

Facter.add(:systemd_release) do
  confine kernel: ['FreeBSD', 'Linux']
  confine service_provider: 'systemd'

  setcode do
    systemd_version = Facter::Util::Resolution.exec('/bin/systemctl --version')
    %r{^systemd\s+(\d+)}.match(systemd_version)[1]
  end
end
