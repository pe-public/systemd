# @summary
#   Regular expression allowing input of tasks maximum value as
#   an explicit number, precentage of DefaultTaskMax or 'infinity'.
#
type Systemd::Tasksmax = Variant[Enum['infinity'], Pattern[/^\d{1,2}%/], Pattern[/^\d+$/]]
