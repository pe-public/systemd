# @summary
#   Regular expression allowing input of quotas in precents.
#   Quotas may exceed 100%.
#
type Systemd::CPUQuota = Pattern[/^\d+%/]
