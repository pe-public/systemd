# @summary
#   Regular expression allowing input of memory limits in either
#   number of bytes (no suffix), kilobytes (K), megabytes (M),
#   gigabytes (G) or terabytes (T) or as a percentage value.
#
type Systemd::Memory = Variant[Pattern[/^\d{1,2}%/], Pattern[/^\d+(K|M|G|T)?$/]]
