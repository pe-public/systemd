# @summary
#   Set the state of a systemd unit.
#
# @param name
#   The full name of the unit including extension.
#
# @param state
#   The desired state of the unit.
#
# @param refreshonly
#   If `true`, the resource has to be notified for it to set a state.
#   If `false`, the state is enforced on each puppet run.
#
define systemd::unit (
  Enum['enabled','active','disabled','masked','absent'] $state = 'enabled',
  Boolean $refreshonly = true,
) {

  case $state {
    'enabled','active': {
      $start_unit = $state ? {
        'active' => " && systemctl start ${name}",
        default   => ''
      }

      exec { "systemd-enable-unit-${name}":
        path        => ['/bin', '/usr/bin'],
        command     => "systemctl enable ${name}${start_unit}",
        onlyif      => "test `systemctl is-enabled ${name}` == 'disabled'",
        refreshonly => $refreshonly,
      }
        
    }
    'disabled': {
      exec { "systemd-disable-unit-${name}":
        path    => ['/bin', '/usr/bin'],
        command => "systemctl disable ${name} && systemctl stop ${name}",
        onlyif => "test ! `systemctl is-enabled ${name}` == 'disabled'",
        refreshonly => $refreshonly,
      }
    }
    'masked': {
      exec { "systemd-mask-unit-${name}":
        path    => ['/bin', '/usr/bin'],
        command => "systemctl disable ${name} && systemctl stop ${name}",
        onlyif => "test ! `systemctl is-enabled ${name}` == 'masked'",
        refreshonly => $refreshonly,
      }
    }
    'absent': {
      exec { "systemd-remove-unit-${name}":
        path    => ['/bin', '/usr/bin'],
        command => "systemctl stop ${name} && systemctl cat ${name} | head -n 1 | cut -d ' ' -f 2- | xargs rm -f",
        onlyif  => "test -f `systemctl cat ${name} | head -n 1 | cut -d ' ' -f 2-`",
        notify  => Exec['systemd-reset-failed'],
        refreshonly => $refreshonly,
      }

      exec { "systemd-reset-failed":
        path        => ['/bin', '/usr/bin'],
        command     => 'systemctl reset-failed && systemctl daemon-reload',
        refreshonly => true,
      }
    }
    default: {}
  }
}
