define systemd::timer::dropin (
                        $ensure                          = 'present',
                        $dropin_order                    = '99',
                        $dropin_name                     = 'override',
                        $purge_dropin_dir                = true,
                        $timer_name                      = $name,
                        $on_active_sec                   = undef,
                        $on_boot_sec                     = undef,
                        $on_startup_sec                  = undef,
                        $on_unit_active_sec              = undef,
                        $on_unit_inactive_sec            = undef,
                        $on_calendar                     = undef,
                        $accuracy_sec                    = undef,
                        $randomized_delay_sec            = undef,
                        $unit                            = undef,
                        $persistent                      = undef,
                        $wake_system                     = undef,
                        $remain_after_elapse             = undef,
                        # unit
                        $description                     = undef,
                        $documentation                   = undef,
                        $wants                           = [],
                        $after                           = undef,
                        $after_units                     = [],
                        $before_units                    = [],
                        $requires                        = [],
                        $binds_to                        = [],
                        $conflicts                       = [],
                        $on_failure                      = [],
                        $partof                          = undef,
                        $allow_isolate                   = undef,
                        $condition_path_is_symbolic_link = undef,
                        $default_dependencies            = undef,
                        # install
                        $also                            = [],
                        $default_instance                = undef,
                        $service_alias                   = [],
                        $wantedby                        = undef,
                        $requiredby                      = [],
                        # added by AT
                        $requires_mounts_for             = [],
                      ) {
  # Timer section
  # if ($persistent)
  # {
  #   validate_bool($persistent)
  # }
  #
  # if ($wake_system)
  # {
  #   validate_bool($wake_system)
  # }
  #
  # if ($remain_after_elapse)
  # {
  #   validate_bool($remain_after_elapse)
  # }
  #
  # # Unit section
  # if ($documentation)
  # {
  #   validate_re(
  #     $documentation,
  #     [ '^https?://', '^file:', '^info:', '^man:'],
  #     "Not a supported documentation uri: ${documentation} - It has to be one of 'http://', 'https://', 'file:', 'info:' or 'man:'")
  # }

  contain systemd

  if ($persistent != undef and $persistent == true and $on_calendar == undef)
  {
    fail('$persistent being "true" only works with $on_calendar being set.')
  }

  $dropin = true

  concat { "/etc/systemd/system/${timer_name}.timer.d/${dropin_order}-${dropin_name}.conf":
    ensure => $ensure,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    notify => Exec['systemctl daemon-reload'],
  }

  concat::fragment { "timer dropin ${timer_name} ${dropin_name} unit":
    target  => "/etc/systemd/system/${timer_name}.timer.d/${dropin_order}-${dropin_name}.conf",
    order   => '00',
    content => template("${module_name}/section/unit.erb"),
  }

  concat::fragment { "timer ${timer_name} install":
    target  => "/etc/systemd/system/${timer_name}.timer.d/${dropin_order}-${dropin_name}.conf",
    order   => '01',
    content => template("${module_name}/section/install.erb"),
  }

  concat::fragment { "timer ${timer_name} timer":
    target  => "/etc/systemd/system/${timer_name}.timer.d/${dropin_order}-${dropin_name}.conf",
    order   => '02',
    content => template("${module_name}/section/timer.erb"),
  }

  if(!defined(File["/etc/systemd/system/${timer_name}.timer.d/"]))
  {
    file { "/etc/systemd/system/${timer_name}.timer.d/":
      ensure  => 'directory',
      owner   => 'root',
      group   => 'root',
      mode    => '0755',
      purge   => $purge_dropin_dir,
      recurse => $purge_dropin_dir,
      notify  => Exec['systemctl daemon-reload'],
    }
  }
}
