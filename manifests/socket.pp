# [Socket]
# ListenStream=80
# ListenStream=0.0.0.0:80
# After=network.target
# Requires=network.target
#
# [Install]
# WantedBy=sockets.target
define systemd::socket(
                        $ensure                          = 'present',
                        $listen_stream                   = undef,
                        $listen_datagram                 = undef,
                        $socket_name                     = $name,
                        $accept                          = undef,
                        # install
                        $also                            = [],
                        $default_instance                = undef,
                        $service_alias                   = [],
                        $wantedby                        = [ 'multi-user.target' ],
                        $requiredby                      = [],
                        # unit
                        $description                     = undef,
                        $documentation                   = undef,
                        $wants                           = [],
                        $after                           = undef,
                        $after_units                     = [],
                        $before_units                    = [],
                        $requires                        = [],
                        $requires_mounts_for             = [],
                        $binds_to                        = [],
                        $conflicts                       = [],
                        $on_failure                      = [],
                        $partof                          = undef,
                        $allow_isolate                   = undef,
                        $condition_path_is_symbolic_link = undef,
                        $default_dependencies            = undef,
                        # added by AT
                        $socket_user                     = 'root',
                        $socket_group                    = 'root',
                        $socket_mode                     = '0644',
                        # resource_control
                        $allowed_memory_nodes            = undef,
                        $cpu_accounting                  = undef,
                        $cpu_quota                       = undef,
                        $cpu_quota_period_sec            = undef,
                        $cpu_startup_weight              = undef,
                        $cpu_weight                      = undef,
                        $delegate                        = undef,
                        $device_allow                    = undef,
                        $device_policy                   = undef,
                        $disable_controllers             = undef,
                        $io_accounting                   = undef,
                        $io_device_weight                = undef,
                        $io_weight                       = undef,
                        $ip_accounting                   = undef,
                        $ip_address_allow                = undef,
                        $memory_accounting               = undef,
                        $memory_high                     = undef,
                        $memory_low                      = undef,
                        $memory_max                      = undef,
                        $memory_min                      = undef,
                        $memory_swap_max                 = undef,
                        $restrict_network_interfaces     = undef,
                        $slice                           = undef,
                        $socket_bind_allow               = undef,
                        $socket_bind_deny                = undef,
                        $startup_allowed_memory_nodes    = undef,
                        $tasks_accounting                = undef,
                        $tasks_max                       = undef,
                        # deprecated in systemd 252
                        $cpu_shares                      = undef,
                        $memory_limit                    = undef,
                      ) {

  contain systemd

  concat { "/etc/systemd/system/${socket_name}.socket":
    ensure => $ensure,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    notify => Exec['systemctl daemon-reload'],
  }

  concat::fragment { "socket ${socket_name} unit":
    target  => "/etc/systemd/system/${socket_name}.socket",
    order   => '00',
    content => template("${module_name}/section/unit.erb"),
  }

  concat::fragment { "socket ${socket_name} install":
    target  => "/etc/systemd/system/${socket_name}.socket",
    order   => '01',
    content => template("${module_name}/section/install.erb"),
  }

  concat::fragment { "socket ${socket_name} socket":
    target  => "/etc/systemd/system/${socket_name}.socket",
    order   => '02',
    content => template("${module_name}/section/socket.erb"),
  }

  concat::fragment { "socket ${socket_name} resource control":
    target  => "/etc/systemd/system/${socket_name}.socket",
    order   => '03',
    content => template("${module_name}/section/slice.erb"),
  }
}
