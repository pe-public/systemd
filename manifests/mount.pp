#
# [Unit]
# Description=Things devices
# After=network.target
#
# [Mount]
# What=172.16.24.192:/mnt/things
# Where=/mnt/things
# Type=nfs
# Options=_netdev,auto
#
# [Install]
# WantedBy=multi-user.target
#
define systemd::mount(
                        $what,
                        $where                           = $name,
                        $type                            = undef,
                        $options                         = [],
                        # install
                        $also                            = [],
                        $default_instance                = undef,
                        $service_alias                   = [],
                        $wantedby                        = [ 'multi-user.target' ],
                        $requiredby                      = [],
                        # unit
                        $description                     = undef,
                        $documentation                   = undef,
                        $wants                           = [],
                        $after                           = undef,
                        $after_units                     = [],
                        $before_units                    = [],
                        $requires                        = [],
                        $binds_to                        = [],
                        $conflicts                       = [],
                        $on_failure                      = [],
                        $partof                          = undef,
                        $allow_isolate                   = undef,
                        $condition_path_is_symbolic_link = undef,
                        $default_dependencies            = undef,
                        $requires_mounts_for             = [],
                        # global
                        $ensure                          = 'present',
                        # resource_control
                        $allowed_memory_nodes            = undef,
                        $cpu_accounting                  = undef,
                        $cpu_quota                       = undef,
                        $cpu_quota_period_sec            = undef,
                        $cpu_startup_weight              = undef,
                        $cpu_weight                      = undef,
                        $delegate                        = undef,
                        $device_allow                    = undef,
                        $device_policy                   = undef,
                        $disable_controllers             = undef,
                        $io_accounting                   = undef,
                        $io_device_weight                = undef,
                        $io_weight                       = undef,
                        $ip_accounting                   = undef,
                        $ip_address_allow                = undef,
                        $memory_accounting               = undef,
                        $memory_high                     = undef,
                        $memory_low                      = undef,
                        $memory_max                      = undef,
                        $memory_min                      = undef,
                        $memory_swap_max                 = undef,
                        $restrict_network_interfaces     = undef,
                        $slice                           = undef,
                        $socket_bind_allow               = undef,
                        $socket_bind_deny                = undef,
                        $startup_allowed_memory_nodes    = undef,
                        $tasks_accounting                = undef,
                        $tasks_max                       = undef,
                        # deprecated in systemd 252
                        $cpu_shares                      = undef,
                        $memory_limit                    = undef,
                      ) {

  contain systemd

  $mount_name = regsubst(regsubst($where, '/', '-', 'G'), '^-', '', '')

  concat { "/etc/systemd/system/${mount_name}.mount":
    ensure => $ensure,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    notify => Exec['systemctl daemon-reload'],
  }

  concat::fragment { "mount ${mount_name} unit":
    target  => "/etc/systemd/system/${mount_name}.mount",
    order   => '00',
    content => template("${module_name}/section/unit.erb"),
  }

  concat::fragment { "mount ${mount_name} resource control":
    target  => "/etc/systemd/system/${mount_name}.mount",
    order   => '01',
    content => template("${module_name}/section/slice.erb"),
  }

  concat::fragment { "mount ${mount_name} install":
    target  => "/etc/systemd/system/${mount_name}.mount",
    order   => '02',
    content => template("${module_name}/section/install.erb"),
  }

  concat::fragment { "mount ${mount_name} mount":
    target  => "/etc/systemd/system/${mount_name}.mount",
    order   => '03',
    content => template("${module_name}/section/mount.erb"),
  }
}
