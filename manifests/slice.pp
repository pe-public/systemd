# puppet2sitepp @systemd.resource-control
define systemd::slice (
                          $ensure                          = 'present',
                          $use_dbus                        = false,
                          $slicename                       = $name,
                          $description                     = undef,
                          $documentation                   = undef,
                          $wants                           = [],
                          $after                           = undef,
                          $after_units                     = [],
                          $before_units                    = [],
                          $requires                        = [],
                          $binds_to                        = [],
                          $conflicts                       = [],
                          $on_failure                      = [],
                          $partof                          = undef,
                          $allow_isolate                   = undef,
                          $condition_path_is_symbolic_link = undef,
                          $default_dependencies            = undef,
                          $requires_mounts_for             = [],
                          # resource_control
                          $allowed_memory_nodes            = undef,
                          $cpu_accounting                  = undef,
                          $cpu_quota                       = undef,
                          $cpu_quota_period_sec            = undef,
                          $cpu_startup_weight              = undef,
                          $cpu_weight                      = undef,
                          $delegate                        = undef,
                          $device_allow                    = undef,
                          $device_policy                   = undef,
                          $disable_controllers             = undef,
                          $io_accounting                   = undef,
                          $io_device_weight                = undef,
                          $io_weight                       = undef,
                          $ip_accounting                   = undef,
                          $ip_address_allow                = undef,
                          $memory_accounting               = undef,
                          $memory_high                     = undef,
                          $memory_low                      = undef,
                          $memory_max                      = undef,
                          $memory_min                      = undef,
                          $memory_swap_max                 = undef,
                          $restrict_network_interfaces     = undef,
                          $slice                           = undef,
                          $socket_bind_allow               = undef,
                          $socket_bind_deny                = undef,
                          $startup_allowed_memory_nodes    = undef,
                          $tasks_accounting                = undef,
                          $tasks_max                       = undef,
                          # deprecated in systemd 252
                          $cpu_shares                      = undef,
                          $memory_limit                    = undef,
                        ) {
  include systemd

  $slicename_filtered = regsubst(regsubst(regsubst($slicename, '/', '-', 'G'), '^-', '', ''), '@', '_at_', 'G')

  if $use_dbus { $unit_location = 'system.control' } else { $unit_location = 'system' }

  concat { "/etc/${unit_location}/system/${slicename}.slice":
    ensure => $ensure,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    notify => Exec['systemctl daemon-reload'],
  }

  concat::fragment { "slice ${slicename_filtered} unit":
    target  => "/etc/${unit_location}/system/${slicename}.slice",
    order   => '00',
    content => template("${module_name}/section/unit.erb"),
  }

  concat::fragment { "slice ${slicename_filtered} slice header":
    target  => "/etc/${unit_location}/system/${slicename}.slice",
    order   => '01',
    content => "[Slice]\n",
  }

  concat::fragment { "slice ${slicename_filtered} slice":
    target  => "/etc/${unit_location}/system/${slicename}.slice",
    order   => '02',
    content => template("${module_name}/section/slice.erb"),
  }
}
