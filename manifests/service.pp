# puppet2sitepp @systemdservices
define systemd::service (
                          $ensure                          = 'present',
                          $servicename                     = $name,
                          $execstart                       = undef,
                          $execstop                        = undef,
                          $execstoppre                     = undef,
                          $execstoppost                    = undef,
                          $execreload                      = undef,
                          $execstartpre                    = undef,
                          $execstartpost                   = undef,
                          $restart                         = undef,
                          $user                            = 'root',
                          $group                           = 'root',
                          $forking                         = false,
                          $pid_file                        = undef,
                          $remain_after_exit               = undef,
                          $type                            = undef,
                          $env_vars                        = [],
                          $unset_env_vars                  = [],
                          $environment_files               = [],
                          $permissions_start_only          = undef,
                          $timeoutstartsec                 = undef,
                          $timeoutstopsec                  = undef,
                          $timeoutsec                      = undef,
                          $restart_prevent_exit_status     = undef,
                          $limit_memlock                   = undef,
                          $limit_nofile                    = undef,
                          $limit_nproc                     = undef,
                          $limit_nice                      = undef,
                          $limit_core                      = undef,
                          $runtime_directory               = undef,
                          $runtime_directory_mode          = undef,
                          $restart_sec                     = undef,
                          $private_tmp                     = false,
                          $working_directory               = undef,
                          $root_directory                  = undef,
                          $umask                           = undef,
                          $nice                            = undef,
                          $oom_score_adjust                = undef,
                          $startlimitinterval              = undef,
                          $startlimitburst                 = undef,
                          $standard_input                  = undef,
                          $standard_output                 = undef,
                          $standard_error                  = undef,
                          $syslog_facility                 = undef,
                          $syslog_identifier               = undef,
                          $killmode                        = undef,
                          $cpuquota                        = undef,
                          $tasksmax                        = undef,
                          $successexitstatus               = [],
                          $killsignal                      = undef,
                          $capability_bounding_set         = [],
                          # install
                          $also                            = [],
                          $default_instance                = undef,
                          $service_alias                   = [],
                          $wantedby                        = [ 'multi-user.target' ],
                          $requiredby                      = [],
                          # unit
                          $description                     = undef,
                          $documentation                   = undef,
                          $wants                           = [],
                          $after                           = undef,
                          $after_units                     = [],
                          $before_units                    = [],
                          $requires                        = [],
                          $binds_to                        = [],
                          $conflicts                       = [],
                          $on_failure                      = [],
                          $partof                          = undef,
                          $allow_isolate                   = undef,
                          $condition_path_is_symbolic_link = undef,
                          $default_dependencies            = undef,
                          $requires_mounts_for             = [],
                          # added by AT
                          $condition_path_exists           = undef,
                          $ambient_capabilities            = undef,
                          $cache_directory                 = undef,
                          $no_new_privileges               = undef,
                          $cache_directory_mode            = undef,
                          $read_write_paths                = undef,
                          $protect_system                  = undef,
                          $restart_force_exit_status       = undef,
                          $limit_rtprio                    = undef,
                          # resource_control
                          $allowed_memory_nodes            = undef,
                          $cpu_accounting                  = undef,
                          $cpu_quota                       = undef,
                          $cpu_quota_period_sec            = undef,
                          $cpu_startup_weight              = undef,
                          $cpu_weight                      = undef,
                          $delegate                        = undef,
                          $device_allow                    = undef,
                          $device_policy                   = undef,
                          $disable_controllers             = undef,
                          $io_accounting                   = undef,
                          $io_device_weight                = undef,
                          $io_weight                       = undef,
                          $ip_accounting                   = undef,
                          $ip_address_allow                = undef,
                          $memory_accounting               = undef,
                          $memory_high                     = undef,
                          $memory_low                      = undef,
                          $memory_max                      = undef,
                          $memory_min                      = undef,
                          $memory_swap_max                 = undef,
                          $restrict_network_interfaces     = undef,
                          $slice                           = undef,
                          $socket_bind_allow               = undef,
                          $socket_bind_deny                = undef,
                          $startup_allowed_memory_nodes    = undef,
                          $tasks_accounting                = undef,
                          $tasks_max                       = undef,
                          # deprecated in systemd 252
                          $cpu_shares                      = undef,
                          $memory_limit                    = undef,
                        ) {
  include systemd

  if($type!=undef and $forking==true)
  {
    fail('Incompatible options: type / forking')
  }

  $servicename_filtered = regsubst(regsubst(regsubst($servicename, '/', '-', 'G'), '^-', '', ''), '@', '_at_', 'G')

  concat { "/etc/systemd/system/${servicename}.service":
    ensure => $ensure,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    notify => Exec['systemctl daemon-reload'],
  }

  concat::fragment { "service ${servicename_filtered} unit":
    target  => "/etc/systemd/system/${servicename}.service",
    order   => '00',
    content => template("${module_name}/section/unit.erb"),
  }

  concat::fragment { "service ${servicename_filtered} service":
    target  => "/etc/systemd/system/${servicename}.service",
    order   => '01',
    content => template("${module_name}/section/service.erb"),
  }

  concat::fragment { "service ${servicename_filtered} resource control":
    target  => "/etc/systemd/system/${servicename}.service",
    order   => '02',
    content => template("${module_name}/section/slice.erb"),
  }

  concat::fragment { "service ${servicename_filtered} install":
    target  => "/etc/systemd/system/${servicename}.service",
    order   => '03',
    content => template("${module_name}/section/install.erb"),
  }
}
